extends Control

var board

func _ready():
	set_process_input(true)
	board = get_tree().get_root().get_node("Game/Board")

func _input_event(event):
	if(get_tree().get_root().get_node("Game/GameHandler").gameState.name == "placeroom"):
		board.HandleInput(event)