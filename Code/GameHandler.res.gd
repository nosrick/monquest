extends Node2D

var buildingStateScript = preload("BuildingState.gd")
var fightingStateScript = preload("FightingState.gd")
var levelUpStateScript = preload("LevelUpState.gd")
var placeRoomStateScript = preload("PlaceRoomState.gd")
var treasureStateScript = preload("TreasureState.gd")
 
var players = []
 
var firstPlayerHealth 
var secondPlayerHealth 
var thirdPlayerHealth 
var fourthPlayerHealth 

var buildingTurn = 0

#The enemy info panel
var panel

#The player info panel
var playerPanel

#The current room that is being used
var currentRoom

#The current card being played
var currentCard

#The level of the current treasure node being explored
var treasureLevel

#The state of the game
var gameState

#Shortcut to the playing field, where cards and stuff will be laid down
var playingField

#For textures
var textureLoader

#For sounds
var soundLoader
var soundPlayer

#Shortcut to the initiative bar
var initiativeBar

#The board handler
var boardHandler
 
func _ready(): 
	set_process(true) 
	set_process_input(true)
	
	print("Loading textures.")
	textureLoader = load("res://Code/TextureLoader.gd").new()
	textureLoader.Load("res://Content/Sprites/Dumped")
	print("Textures loaded.")
	
	print("Loading sounds.")
	soundLoader = load("res://Code/SoundLoader.gd").new()
	soundLoader.Load()
	print("Sounds loaded.")
	
	randomize()
		
	soundPlayer = get_tree().get_root().get_node("Game/SoundEffects")
	var classLoader = load("res://Code/ClassLoader.gd").new()
	classLoader.soundPlayer = soundPlayer
	classLoader.LoadDefaults()
	
	print("Creating players")
	players.push_back(classLoader.warrior)
	players.push_back(classLoader.thief)
	players.push_back(classLoader.wizard)
	players.push_back(classLoader.cleric)
	print("Players created.")
	 
	#Set up the click points
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Warrior/Click").SetPlayer(players[0])
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Warrior/Click").gameHandler = self
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Thief/Click").SetPlayer(players[1]) 
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Thief/Click").gameHandler = self
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Wizard/Click").SetPlayer(players[2])
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Wizard/Click").gameHandler = self
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Cleric/Click").SetPlayer(players[3]) 
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Cleric/Click").gameHandler = self
	   
	#Set up the health bars
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Warrior/HealthBackground/HealthBar").SetPlayer(players[0]) 
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Thief/HealthBackground/HealthBar").SetPlayer(players[1]) 
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Wizard/HealthBackground/HealthBar").SetPlayer(players[2]) 
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Cleric/HealthBackground/HealthBar").SetPlayer(players[3]) 

	#Set up the health text
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Warrior/HealthText").entity = players[0]
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Thief/HealthText").entity = players[1]
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Wizard/HealthText").entity = players[2]
	get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/Cleric/HealthText").entity = players[3]

	panel = get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel")
	playingField = get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayingField")
	initiativeBar = get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/InitiativeBar")
	initiativeBar.gameHandler = self
	
	boardHandler = get_tree().get_root().get_node("Game/Board")
	boardHandler.Initialise()
	
	print("Entering first Build state.")
	gameState = buildingStateScript.new()
	gameState.Enter(self, playingField)
	gameState.DisplayCards()

 
func _process(delta):
	gameState.Process(delta)
	initiativeBar.Update(delta)
	
func _input(event): 
	gameState.HandleInput(event)
	

func ChangeState(newState):
	gameState.Exit()
	#gameState.free()
	gameState = newState
	gameState.Enter(self, playingField)
	gameState.DisplayCards()