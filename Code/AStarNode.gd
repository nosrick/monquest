extends Object

var parent = null
var position = Vector2(-1, -1)
var successors = []
var cost = 9000

func _init(parentRef, positionRef, costRef):
	parent = parentRef
	position = positionRef
	cost = costRef
	
func GetSuccessors():
	var successors = []
	for i in range(position.x - 1, position.x + 2, 1):
		for j in range(position.y - 1, position.y + 2, 1):
			if(i != position.x or j != position.y):
				successors.append(get_script().new(self, Vector2(i, j), cost + 1))
	
	return successors
	
func Calculate(goalNode):
	if(position != goalNode):
		var xD = position.x - goalNode.position.x
		var yD = position.y - goalNode.position.y
		return xD + yD