extends Control

var user
var target
var script
var gameHandler

func _ready():
	set_process_input(true)

func _input_event(event):
	if(event.is_action_pressed("left_click") and self.gameHandler != null):
		if(gameHandler.gameState.currentPlayer != null):
			gameHandler.gameState.currentPlayer.attackDeck.push_back(script)
			gameHandler.gameState.treasures.erase(script)
			gameHandler.gameState.DisplayCards()
			get_parent().queue_free()