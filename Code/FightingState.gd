extends "GameState.gd"

var currentRoom

func _init(room):
	self.currentRoom = room
	self.name = "fighting"

func Enter(gameHandler, playingField):
	.Enter(gameHandler, playingField)
	
	currentRoom.SetGameHandler(gameHandler)
	currentRoom.SetPlayingField(playingField)
	currentRoom.GenerateEntities()
	
	for i in gameHandler.players:
		i.HealMe(i.HP)
	
	gameHandler.currentRoom = currentRoom
	var panel = gameHandler.panel
	EnterRoom()

func DisplayCards():
	currentRoom.DisplayCards(currentRoom.GetCurrentPlayer())
	
func EnterRoom():
	currentRoom.Enter(gameHandler.players, currentRoom.rightEntities)
	
func Process(delta):
	var panel = gameHandler.panel
	#panel.set_opacity(1)
	
	currentRoom.Process(delta)
	
	if(currentRoom.finished):
		gameHandler.currentRoom = null
		var levelUp = false
		for i in gameHandler.players:
			if(i.isLeveling):
				levelUp = true
				break
		
		gameHandler.ChangeState(gameHandler.treasureStateScript.new(gameHandler.treasureLevel, currentRoom.level, levelUp))
	
func Exit():
	var panel = gameHandler.panel
	for i in panel.get_children():
		if(i.get_name().find("BasicMonster") != -1):
			i.queue_free()
	
	for i in gameHandler.initiativeBar.get_children():
		i.queue_free()
	
	finished = true