extends Sprite

var entity
var initialPosition = self.get_pos()
var initialScale = self.get_scale()

var lastHealth
var lastScale = initialScale

var lerpTick = 0

func _ready():
	set_process(true)

func _process(delta):	
	var scale = self.get_scale()
	
	if(entity == null):
		return
	
	if(entity.HPRemaining != lastHealth):
		lerpTick = 0
		
	#print(get_path())
	self.set_scale(Vector2(scale.width, lerp(lastScale.height, (self.initialScale.height * (float(self.entity.HPRemaining) / float(self.entity.HP))), lerpTick)))
	
	self.set_pos(Vector2(self.initialPosition.x, self.initialPosition.y + self.initialScale.height - scale.y))
	
	lerpTick = min(1, lerpTick + delta)
	lastScale = scale
	lastHealth = entity.HPRemaining
	
func SetPlayer(entity):
	self.entity = entity