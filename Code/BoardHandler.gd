extends Node

var Pathfinder = preload("FillPathfinder.gd")

var roomResource = ResourceLoader.load("tile.scn")
var roomCursor = ResourceLoader.load("tile.scn").instance()

var treasureResource = load("TreasurePoint.scn")

var size = 16
var mapSize = 50

var lastRoom = Vector2(3, 3)
var roomSize = Vector2(3, 3)

var grid = {}

var placing = false

var treasurePoints = {}

func Initialise():
	for i in range(0, mapSize):
		for j in range(0, mapSize):
			grid[Vector2(i, j)] = false
			
	var lastPoint = Vector2(mapSize / 2, mapSize / 2)
	treasurePoints[(Vector2(mapSize / 2, mapSize / 2))] = (randi() % 4)
	for i in range(0, mapSize / 3):
		while(true):
			var x = min(mapSize - 2, randi() % mapSize + 2)
			var y = min(mapSize - 2, randi() % mapSize + 2)
			if(!CheckAdjacentTreasure(Vector2(x, y), Vector2(5, 5))):
				treasurePoints[(Vector2(x, y))] = (randi() % 4)
				break
				
	for i in treasurePoints.keys():
		var point = treasureResource.instance()
		point.set_pos(TileToWorldCoord(i))
		if(treasurePoints[i] == 0):
			point.set_modulate(Color(0.3, 0.8, 0.0, 1.0))
		elif(treasurePoints[i] == 1):
			point.set_modulate(Color(0.8, 0.8, 0.0, 1.0))
		elif(treasurePoints[i] == 2):
			point.set_modulate(Color(0.8, 0.4, 0.0, 1.0))
		elif(treasurePoints[i] == 3):
			point.set_modulate(Color(0.8, 0.0, 0.0, 1.0))
		add_child(point)

func Process(delta):
	var mousePosition = get_global_mouse_pos()
	roomCursor.set_global_pos(SnapToGrid(mousePosition))
	
func HandleInput(event):
	if(event.is_action_released("left_click") and placing):
		#var tileCoord = WorldToTileCoord(roomCursor.get_global_pos())
		var tileCoord = WorldToTileCoord((get_global_mouse_pos()))
		
		if(tileCoord.x < -mapSize or tileCoord.x > mapSize - 1 or tileCoord.y < -mapSize or tileCoord.y > mapSize - 1):
			return
			
		if(!treasurePoints.has(tileCoord)):
			return
		
		if(!CheckAdjacent(tileCoord, roomSize)):
			for i in range(tileCoord.x - (roomSize.x / 2) + 1, tileCoord.x + (roomSize.x / 2) + 1):
				for j in range(tileCoord.y - (roomSize.y / 2) + 1, tileCoord.y + (roomSize.y / 2) + 1):
					CreateTile(Vector2(i, j))
				
			DigCorridor(lastRoom, tileCoord)
			lastRoom = tileCoord
			placing = false
			get_tree().get_root().get_node("Game/GameHandler").treasureLevel = treasurePoints[tileCoord]
	

func Setup():
	placing = true
	var children = roomCursor.get_children()
	for i in range(0, children.size()):
		children[i].free()
		
	roomCursor = roomResource.instance()
	roomCursor.set_scale(Vector2(1, 1))
	var roomPos = WorldToTileCoord(roomCursor.get_pos())
	add_child(roomCursor) 
	for i in range(-1, roomSize.x - 1, 1):
		for j in range(-1, roomSize.y - 1, 1):
			var tile = roomResource.instance()
			roomCursor.add_child(tile)
			tile.set_pos(TileToWorldCoord(Vector2(i, j)))
			tile.set_scale(Vector2(1, 1))
			tile.set_modulate(Color(1.0, 1.0, 1.0, 0.3))
		
	
	
func WorldToTileCoord(pos):
	var x = pos.x
	var y = pos.y
	x = int(floor(x / size))
	y = int(floor(y / size))
	return Vector2(x, y)	

func WorldToTileCoord2(pos):
	var x = pos.x
	var y = pos.y
	x = int(x / (size * 2))
	y = int(y / (size * 2))
	return Vector2(x, y)
	
func TileToWorldCoord2(pos):
	var x = pos.x
	var y = pos.y
	x = x * size * 2
	y = y * size * 2
	return Vector2(x, y)
	
func TileToWorldCoord(pos):
	var x = pos.x
	var y = pos.y
	x = int(x * size)
	y = int(y * size)
	return Vector2(x, y)
	
func TileToWorldCoordHalf(pos):
	var x = pos.x
	var y = pos.y
	x = x * size / 2
	y = y * size / 2
	return Vector2(x, y)

func CheckAdjacent(pos, size):
	var occupied = false
	for i in range(pos.x - (size.x / 2), pos.x + 1 + (size.x / 2), 1):
		for j in range(pos.y - (size.y / 2), pos.y + 1 + (size.y / 2), 1):
			if(grid.has(Vector2(i, j))):
				if(grid[Vector2(i, j)] == true):
					occupied = true
					return occupied
			
		
	return occupied
	
func CheckAdjacentTreasure(pos, size):
	var occupied = false
	for i in range(pos.x - (size.x / 2), pos.x + 1 + (size.x / 2), 1):
		for j in range(pos.y - (size.y / 2), pos.y + 1 + (size.y / 2), 1):
			if(treasurePoints.has(Vector2(i, j))):
				occupied = true
				return occupied
			
		
	return occupied

func DigCorridor(begin, end):
	var pathfinder = Pathfinder.new()
	pathfinder.Initialise(mapSize)
	pathfinder.FloodFill(begin, end)
	var path = pathfinder.FindPath(begin, end)
	if(path == null):
		return
	
	for node in path:
		CreateTile(node)


func CreateTile(point):
	if(grid.has(point)):
		if(grid[point] == false):
			grid[point] = true
			var tile = roomResource.instance()
			add_child(tile)
			tile.set_pos(TileToWorldCoord(point))
			tile.set_scale(Vector2(1, 1))
		
	

func SnapToGrid(pos):
	var x = pos.x
	var y = pos.y
	x = int((x - (int(x) % size)))
	y = int((y - (int(y) % size)))
	return Vector2(x, y)