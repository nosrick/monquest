extends Control

var player
var gameHandler

var shaking
var shakeCounter
var shakeIntensity
var initialPosition

var hitParticles = preload("../HitParticles.scn").instance()
var selectParticles = preload("../HitParticles.scn").instance()

const SHAKE_MULTIPLIER = 4
const SHAKE_DURATION = 0.5
const PARTICLES = 32

func _ready():
	set_process(true)
	set_process_input(true)
	shaking = false
	shakeCounter = 0
	shakeIntensity = 0
	connect("mouse_exit", self, "ResetColour")
	
	add_child(selectParticles)
	selectParticles.set_scale(Vector2(0.1, 0.1))
	selectParticles.set_amount(PARTICLES)
	
	add_child(hitParticles)
	hitParticles.set_global_pos(get_global_pos() + Vector2(40, 40))
	hitParticles.set_scale(Vector2(0.3, 0.3))
	hitParticles.set_draw_behind_parent(true)
	hitParticles.set_emitting(false)

func _process(delta):
	if(shaking):
		shakeCounter += delta
		get_parent().set_global_pos(Vector2(initialPosition.x + (rand_range(-shakeIntensity, shakeIntensity) * SHAKE_MULTIPLIER), initialPosition.y + (rand_range(-shakeIntensity, shakeIntensity) * SHAKE_MULTIPLIER)))
	
	if(shakeCounter >= SHAKE_DURATION):
		shaking = false
		get_parent().set_global_pos(initialPosition)
		shakeCounter = 0
		hitParticles.set_emitting(false)
	
	
func Shake():
	initialPosition = get_parent().get_global_pos()
	shaking = true
	shakeCounter = 0
	shakeIntensity = float(player.HP) / float(player.HPRemaining)
	hitParticles.set_emitting(true)
	hitParticles.set_amount(PARTICLES * (1.1 - (float(player.HPRemaining) / float(player.HP))))

func SetPlayer(player):
	self.player = player
	
func SetGameHandler(gameHandler):
	self.gameHandler = gameHandler
	
func ResetColour():
	selectParticles.set_emitting(false)

func _input_event(event):
	if(player == null):
		return
		
	if(player.HPRemaining == 0):
		return
		
	if(gameHandler == null):
		return
		
		
	for i in gameHandler.initiativeBar.get_children():
		if(i.get_name() == player.name):
			selectParticles.set_global_pos(i.get_global_pos())
			selectParticles.set_emitting(true)
			break
		
	if(!event.is_action_released("left_click")):
		return
		
	if(gameHandler.gameState.name == "fighting"):
		if(gameHandler.currentCard == null):
			return
			
		if(gameHandler.currentCard.user == null):
			return
			
		Shake()
		if(!gameHandler.currentCard.user.isPlayerControlled):
			return
			
		gameHandler.currentCard.SetTarget(player)
		gameHandler.currentCard.Execute()
		var playingField = gameHandler.playingField
		gameHandler.currentRoom.GetCurrentPlayer().hand.remove(gameHandler.currentCard.get_parent().get_position_in_parent())
		playingField.remove_child(gameHandler.currentCard.get_parent())
		gameHandler.currentCard.get_parent().free()
		gameHandler.currentCard = null
		gameHandler.currentRoom.Turn()
	elif(gameHandler.gameState.name == "treasure"):
		gameHandler.gameState.currentPlayer = player