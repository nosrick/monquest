extends Object

var entityResource = preload("Entity.gd")

func ProvideEntities(roomLevel, soundPlayer):
	var entities = []
	var totalLevels = 0
	var attackProvider = load("res://Code/AttackProvider.gd").new()
	var attackHandler = load("res://Code/AttackHandler.gd").new()
	
	var tally = 1
	while(totalLevels < roomLevel * 4):
		#Get the level of the first combatant
		var result = randi() % roomLevel + 1
		
		#Make sure the room isn't too difficult
		if(result + totalLevels > roomLevel * 4):
			continue
		
		#Get their attacks
		var classLoader = load("res://Code/ClassLoader.gd").new()
		classLoader.soundPlayer = soundPlayer
		var entity = classLoader.LoadClass("BasicMonster", false)
		entity.name = "BasicMonster" + str(tally)
		for i in range(result - 1):
			entity.LevelUp()
		
		entities.push_back(entity)
		totalLevels += result
		tally += 1
		
	return entities