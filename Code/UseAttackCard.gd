extends Control

var user
var target
var script
var gameHandler

var lerpTick = 0

func _ready():
	set_process_input(true)
	
func SetTarget(target):
	self.target = target
	
func Execute():
	user.PushBack(script.Execute(user, target))
	user.usedAttacks.push_back(script)
	
func _input_event(event):
	#get_parent().set_scale(Vector2(1.1, 1.1))
	lerpTick += 0.1
	
	if(event.is_action_pressed("left_click") and self.gameHandler != null):
		if(user.isPlayerControlled):
			self.gameHandler.currentCard = self
		else:
			get_tree().get_root().get_node("Game/CameraParent/Camera2D/CanvasLayer/PlayerPanel/" + target.name + "/Click").Shake()
			Execute()
			self.gameHandler.currentRoom.Turn()
			
func _on_mouse_exit():
	get_parent().set_scale(Vector2(1, 1))