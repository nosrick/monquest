extends "BaseAttack.gd"

func _init(name, internalName, pushback, sounds, level, description).(name, internalName, pushback, sounds, level, description):
	pass
	
func CalculatePower(user, target):
	return int(user.level * 3.5)

func Execute(user, target):
	target.DamageMe(CalculatePower(user, target))
	
	return .Execute(user, target)