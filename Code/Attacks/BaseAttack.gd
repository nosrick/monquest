extends Object

var name
var internalName
var pushback
var sounds = []
var level
var description

func _init(name, internalName, pushback, sounds, level, description):
	self.name = name
	self.internalName = internalName
	self.pushback = int(pushback)
	self.sounds = sounds
	self.level = int(level)
	self.description = description
	
func CalculatePower(user, target):
	return 0
	
func Execute(user, target):
	PlaySound(user.soundPlayer)
	return int(pushback)
	
func PlaySound(soundPlayer):
	soundPlayer.play(sounds[randi() % sounds.size()].name)