extends "BaseAttack.gd"

func _init(name, internalName, pushback, sounds, level, description).(name, internalName, pushback, sounds, level, description):
	pass
	
func CalculatePower(user, target):
	if(target == null):
		return -1
	else:
		return target.level * 5

func Execute(user, target):
	target.HealMe(target.level * 5)
	
	return .Execute(user, target)