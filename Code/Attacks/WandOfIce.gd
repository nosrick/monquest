extends "BaseAttack.gd"

func _init(name, internalName, pushback, sounds, level, description).(name, internalName, pushback, sounds, level, description):
	pass
	
func CalculatePower(user, target):
	return user.level + 1

func Execute(user, target):
	target.DamageMe(CalculatePower(user, target))
	target.PushBack(user.level + 2)
	
	return .Execute(user, target)