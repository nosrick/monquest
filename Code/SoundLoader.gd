extends Object

var sounds = {}

func Load(path = "res://Content/Sounds/"):
	var directory = Directory.new()
	var baseDirectory = path
	#print("Got path ", baseDirectory)
	
	directory.change_dir(baseDirectory)
	directory.list_dir_begin()
	var temp = directory.get_next()
	while temp != "":
		if(temp == "." or temp == ".."):
			temp = directory.get_next()
			continue

		#print("Got file, ", temp)
		var extension = temp.substr(temp.find_last("."), 4)
		if(extension == ".wav"):
			var name = temp.substr(0, temp.length() - 4)
			sounds[name] = ResourceLoader.load(baseDirectory + "/" + temp)
			#print("Loaded ", name)
		
		temp = directory.get_next()
	
func Get(name):
	if(sounds.has(name)):
		return sounds[name]