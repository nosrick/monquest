extends "GameState.gd"

var cardResource = preload("res://TreasureCard.scn")

var treasures = []
var levelUp = false
var currentPlayer = null

var lerpTick = 0

func _init(treasureLevel, roomLevel, levelUp):
	var totalLevel = treasureLevel + roomLevel
	self.name = "treasure"
	self.levelUp = levelUp
	GenerateTreasures(totalLevel)

func Enter(gameHandler, playingField):
	.Enter(gameHandler, playingField)

func GenerateTreasures(level):
	var treasureProvider = load("res://Code/TreasureProvider.gd").new()
	var remaining = level
	while(remaining != 0):
		var result = randi() % remaining + 1
		remaining -= result
		treasures.push_back(treasureProvider.GetTreasureOfLevel(result))

func Process(delta):
	if(treasures.size() == 0):
		if(levelUp):
			gameHandler.ChangeState(gameHandler.levelUpStateScript.new())
		else:
			gameHandler.ChangeState(gameHandler.buildingStateScript.new())
		return
	
	lerpTick = min(1, lerpTick + delta * 2)
	
	if(playingField != null):
		for i in range(playingField.get_child_count()):
			var width = playingField.get_child(i).get_node("CardBackground").get_margin(MARGIN_RIGHT)
			playingField.get_child(i).set_pos(Vector2(lerp(0, i * width + (width / 2) + i, lerpTick), lerp(1000, 150, lerpTick)))
	
func DisplayCards():
	lerpTick = 0
	
	for i in treasures:
		var card = cardResource.instance()
		var width = card.get_node("CardBackground").get_margin(MARGIN_RIGHT)
		card.get_node("CardBackground/CardForeground/CardName").set_text(i.name)
		
		var powerText = "?"
		if(currentPlayer != null):
			if(i.CalculatePower(currentPlayer, null) == -1):
				powerText = "?"
			else:
				powerText = str(i.CalculatePower(currentPlayer, null))
		
		card.get_node("CardBackground/CardForeground/Power").set_text(powerText)
		card.get_node("CardBackground/CardForeground/Initiative").set_text(str(i.pushback))
		card.get_node("CardBackground/CardForeground/Level").set_text(str(i.level))
		card.get_node("CardBackground/CardForeground/Description").set_text(i.description)
		card.get_node("Click").gameHandler = gameHandler
		card.get_node("Click").script = i
		#card.get_node("Click").user = entity
		if(playingField != null):
			playingField.add_child(card)
	
func HandleInput(event):
	pass
	
func Exit():
	for i in playingField.get_children():
		playingField.get_child(0).free()