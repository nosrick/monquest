extends Node

var rightEntities = []
var entities = []
var deadEntities = []
var reserveEntities = []
var cardResource = ResourceLoader.load("res://attackCard.scn")
var clickResource = preload("../UseAttackCard.gd")
var playingField
var gameHandler
var initiativeHandler
var initiativeSorter = preload("../InitiativeSorter.gd").new()
var entityProvider = preload("../CombatEntityProvider.gd").new()

var level
var enemyCount = 0
	
var enemiesDefeated = 0
var playersDefeated = 0

var finished = false

var name

var entityIndex = 0

var lerpTick = 0

func _init(level, name):
	self.level = level
	self.name = name
	
func GenerateEntities():
	rightEntities = entityProvider.ProvideEntities(level + gameHandler.treasureLevel, gameHandler.soundPlayer)
	
func SetPlayingField(playingField):
	self.playingField = playingField
	
func SetGameHandler(gameHandler):
	self.gameHandler = gameHandler
	self.initiativeHandler = gameHandler.initiativeBar
	
func Enter(leftEntities, rightEntities):
	print("Room level ", self.level)
	finished = false
	enemiesDefeated = 0
	playersDefeated = 0
	enemyCount = rightEntities.size()
	self.rightEntities = rightEntities
	
	for i in leftEntities:
		entities.push_back(i)
		
	for i in range(0, 4, 1):
		entities.push_back(rightEntities[i])
		
	for i in range(4, rightEntities.size(), 1):
		reserveEntities.push_back(rightEntities[i])
	
	for i in entities:
		i.Shuffle(i.attackDeck)
		i.InitialAttackDraw()
		i.SetInitiative(0)
	
	initiativeHandler.Setup(entities)
	entities[entityIndex].AttackDraw()
	#Turn()
	
func DisplayCards(entity):
	lerpTick = 0
	var hand = entity.hand
	for i in hand:
		var card = cardResource.instance()
		var width = card.get_node("CardBackground").get_margin(MARGIN_RIGHT)
		card.get_node("CardBackground/CardForeground/CardName").set_text(i.name)
		
		var powerText = ""
		if(i.CalculatePower(entity, null) == -1):
			powerText = "?"
		else:
			powerText = str(i.CalculatePower(entity, null))
		
		card.get_node("CardBackground/CardForeground/Power").set_text(powerText)
		card.get_node("CardBackground/CardForeground/Initiative").set_text(str(i.pushback))
		card.get_node("CardBackground/CardForeground/Level").set_text(str(i.level))
		card.get_node("CardBackground/CardForeground/Description").set_text(i.description)
		card.get_node("Click").gameHandler = gameHandler
		card.get_node("Click").script = i
		card.get_node("Click").user = entity
		if(playingField != null):
			playingField.add_child(card)
			
func ChooseCard(entity):
	lerpTick = 0
	var result = randi() % entity.hand.size()
	var card = entity.hand[result]
	var cardNode = cardResource.instance()
	cardNode.get_node("CardBackground/CardForeground/CardName").set_text(card.name)
	
	var powerText = ""
	if(card.CalculatePower(entity, null) == -1):
		powerText = "?"
	else:
		powerText = str(card.CalculatePower(entity, null))
		
	cardNode.get_node("CardBackground/CardForeground/Power").set_text(powerText)
	cardNode.get_node("CardBackground/CardForeground/Initiative").set_text(str(card.pushback))
	cardNode.get_node("Click").gameHandler = gameHandler
	cardNode.get_node("Click").script = card
	cardNode.get_node("Click").user = entity
	if(playingField != null):
		playingField.add_child(cardNode)
		
	var targets = []
	for i in entities:
		if(i.isPlayerControlled):
			targets.push_back(i)
			
	var target = randi() % targets.size()
	cardNode.get_node("Click").SetTarget(targets[target])

func Process(delta):
	if(playingField != null):
		for i in range(playingField.get_child_count()):
			var width = playingField.get_child(i).get_node("CardBackground").get_margin(MARGIN_RIGHT)
			playingField.get_child(i).set_pos(Vector2(lerp((entityIndex % 4) * 245, i * width + (width / 2) + i, lerpTick), lerp(1000, 150, lerpTick)))
		if(lerpTick < 1):
			lerpTick = min(1, lerpTick + (delta * 2))
	
func GetCurrentPlayer():
	return entities[entityIndex]

func Turn():
	for i in range(playingField.get_child_count()):
		playingField.get_child(0).free()
	
	for i in entities:
		if(i.HPRemaining == 0):
			if(!i.isPlayerControlled):
				enemiesDefeated += 1
				gameHandler.soundPlayer.play("Odead")
				if(reserveEntities.size() > 0):
					entities.push_back(reserveEntities[0])
					reserveEntities.pop_front()
			else:
				playersDefeated += 1
				gameHandler.soundPlayer.play("Hdead")
		
			initiativeHandler.RemoveEntity(i)
			deadEntities.push_back(i)
			entities.erase(i)
			initiativeHandler.entities = entities
			initiativeHandler.UpdateEntities()
			initiativeHandler.MovePlates()
			
	if(playersDefeated == 4):
		Exit()
		return
	
	if(enemiesDefeated == enemyCount):
		Exit()
		return
	
	entities.sort_custom(initiativeSorter, "Sort")
	initiativeHandler.entities = entities
	initiativeHandler.MovePlates()
	
	entities[entityIndex].AttackDraw()
	if(entities[entityIndex].isPlayerControlled):
		DisplayCards(entities[entityIndex])
	else:
		ChooseCard(entities[entityIndex])
	
	for i in initiativeHandler.entities:
		i.PullForward(1)
	
func Exit():
	for i in entities:
		i.AttackReturn()
		if(i.HPRemaining > 0):
			for j in rightEntities:
				i.AddXP(j.level * 3)
		
	for i in deadEntities:
		i.AttackReturn()
		
	entities.clear()
	deadEntities.clear()
	
	for i in initiativeHandler.get_children():
		i.queue_free()
	
	for i in range(playingField.get_child_count()):
		playingField.get_child(0).queue_free()
		
	finished = true