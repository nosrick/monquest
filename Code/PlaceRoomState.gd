extends "GameState.gd"

var room

func _init(room):
	self.room = room
	self.name = "placeroom"

func Enter(gameHandler, playingField):
	.Enter(gameHandler, playingField)
	gameHandler.boardHandler.Setup()
	
func Process(delta):
	gameHandler.boardHandler.Process(delta)
	
	if(!gameHandler.boardHandler.placing):
		gameHandler.ChangeState(gameHandler.fightingStateScript.new(room))
	
func HandleInput(event):
	pass
	#gameHandler.boardHandler.HandleInput(event)
	
func DisplayCards():
	pass
	
func Exit():
	pass