extends Panel

var entities = []
var initiativeSorter = preload("InitiativeSorter.gd").new()
var gameHandler
var lerpTick = 0
var font = preload("res://Content/Fonts/SmallFont.fnt")

var lastRemoved = 0

var newestPlate = null

func Setup(entities):
	self.entities = []
	for i in range(entities.size()):
		self.entities.push_back(entities[i])
		
	for i in range(4, min(8, self.entities.size())):
		CreatePanelPortrait(entities[i], i)
		
	for i in range(min(8, self.entities.size())):
		var node
		for j in gameHandler.panel.get_children():
			if(j.get_name() == entities[i].name):
				node = j.duplicate()
				CreateInitiativePortrait(node, entities[i])
		
		
	for j in gameHandler.initiativeBar.get_children():
		j.set_scale(Vector2(2, 2))
		
	lerpTick = 0

func Update(delta):
	if(lerpTick == 1):
		newestPlate = null
	
	#Match the entities to the plates
	for i in range(entities.size()):
		for j in get_children():
			if(j.get_name() == entities[i].name):
				move_child(j, i)
				j.set_pos(Vector2(lerp(920, 920 - j.get_position_in_parent() * 130, lerpTick), 0))
				j.get_child(0).set_text(str(entities[i].initiative))
	
	if(newestPlate != null):
		newestPlate.set_pos(Vector2(newestPlate.get_pos().x, lerp(newestPlate.get_pos().y, -470, lerpTick)))
	
	lerpTick = min(1, lerpTick + delta * 1.5)
	
func MovePlates():
	Sort()
	lerpTick = 0
	
func Sort():
	entities.sort_custom(initiativeSorter, "Sort")
	
func RemoveEntity(entity):		
	for i in range(get_child_count()):
		if(entities[i].name == entity.name):
			remove_child(get_child(i))
			for j in gameHandler.panel.get_children():
				if(j.get_name() == entities[i].name and !entities[i].isPlayerControlled):
					lastRemoved = j.get_position_in_parent()
					j.queue_free()
					break
			entities.erase(entity)
			return

func UpdateEntities():
	if(entities.size() == 8):
		
		#Add the entity to the enemy panel
		var temp = CreatePanelPortrait(entities[7], lastRemoved)
		
		var node = temp.duplicate()
		
		CreateInitiativePortrait(node, entities[7])


func CreatePanelPortrait(entity, index):
	var enemyPanel = ResourceLoader.load("res://EnemyPanel.scn").instance()
	enemyPanel.set_name(entity.name)
	enemyPanel.set_pos(Vector2((index - 4) * 245 + 40, -600))
	enemyPanel.get_node("HealthBackground/HealthBar").SetPlayer(entity)
	enemyPanel.get_node("Click").SetPlayer(entity)
	enemyPanel.get_node("Click").SetGameHandler(gameHandler)
	enemyPanel.get_node("HealthText").entity = entity
	
	enemyPanel.set_texture(gameHandler.textureLoader.textures["Undead0"])
	enemyPanel.set_region_rect(Rect2(0, 0, 16, 16))
	
	gameHandler.panel.add_child(enemyPanel)
	
	if(newestPlate != null):
		newestPlate.set_pos(Vector2(newestPlate.get_pos().x, -470))
	
	newestPlate = enemyPanel
	
	return enemyPanel

func CreateInitiativePortrait(node, entity):
	for k in range(node.get_child_count()):
		node.remove_child(node.get_child(0))
		
	node.set_scale(Vector2(2, 2))
	var label = Label.new()
	label.add_font_override("font", font)
	label.set_text(str(entity.initiative))
	label.set_scale(Vector2(0.5, 0.5))
	label.set_pos(Vector2(3, 3))
	node.add_child(label)
	
	gameHandler.initiativeBar.add_child(node)