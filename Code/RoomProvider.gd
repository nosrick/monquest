extends Object

func GetRooms(entityLevel, jsonData):
	var entityData = jsonData
	
	var rooms = []
	
	for i in entityData.entity.rooms:
		var name = i.name
		var internalName = i.internalName
		var level = int(i.level)
		var room = load("res://Code/Rooms/" + internalName + ".gd").new(level, name)
		if(level == 0):
			for j in range(5):
				rooms.push_back(room)
				
		elif(level <= entityLevel):
			rooms.push_back(room)
		
	
	return rooms