
extends Object

var grid = {}
var mapSize = 50

func Initialise(mapSize):
	self.mapSize = mapSize
	
	for i in range(0, mapSize):
		for j in range(0, mapSize):
			grid[Vector2(i, j)] = 9000
		
	

func FloodFill(begin, end):
	
	grid[begin] = 0
	
	var bagOfNodes = []
	
	bagOfNodes.push_back(begin)
	var cost = 0
	while(bagOfNodes.size() > 0):
		var point = bagOfNodes[0]
		bagOfNodes.pop_front()
		if(CheckValidity(point, cost) == false):
			pass
			
		if(CheckValidity(Vector2(point.x - 1, point.y), cost + 1) == true):
			bagOfNodes.push_back(Vector2(point.x - 1, point.y))
			
		if(CheckValidity(Vector2(point.x + 1, point.y), cost + 1) == true):
			bagOfNodes.push_back(Vector2(point.x + 1, point.y))
			
		if(CheckValidity(Vector2(point.x, point.y - 1), cost + 1) == true):
			bagOfNodes.push_back(Vector2(point.x, point.y - 1))
			
		if(CheckValidity(Vector2(point.x, point.y + 1), cost + 1) == true):
			bagOfNodes.push_back(Vector2(point.x, point.y + 1))
			
		cost += 1
	

func CheckValidity(point, cost):
	if(point.x < 0):
		return false
		
	if(point.x > mapSize - 1):
		return false
		
	if(point.y < 0):
		return false
		
	if(point.y > mapSize - 1):
		return false
		
	if(grid[point] != 9000):
		return false
		
	grid[point] = cost
	
	return true

func FindPath(begin, end):
	var solution = []
	
	solution.push_front(end)
	var point = end
	while(true):
		var changed = false
		if(point.x > 0 and changed == false):
			if(grid[Vector2(point.x - 1, point.y)] < grid[point]):
				point.x -= 1
				changed = true
		
		if(point.x < mapSize - 1 and changed == false):
			if(grid[Vector2(point.x + 1, point.y)] < grid[point]):
				point.x += 1
				changed = true
		
		if(point.y > 0 and changed == false):
			if(grid[Vector2(point.x, point.y - 1)] < grid[point]):
				point.y -= 1
				changed = true
		
		if(point.y < mapSize - 1 and changed == false):
			if(grid[Vector2(point.x, point.y + 1)] < grid[point]):
				point.y += 1
				changed = true
			
		solution.push_front(point)
		
		if(point == begin):
			return solution
	
