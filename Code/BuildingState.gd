extends "GameState.gd"

var cardResource = ResourceLoader.load("res://roomCard.scn")

var lerpTick = 0

var entity

func _init():
	self.name = "building"

func Enter(gameHandler, playingField):
	.Enter(gameHandler, playingField)
	
func DisplayCards():
	self.entity = gameHandler.players[gameHandler.buildingTurn]
	entity.InitialRoomDraw()
	var hand = entity.hand
	for i in range(hand.size()):
		var card = cardResource.instance()
		var width = card.get_node("Background").get_margin(MARGIN_RIGHT)
		card.get_node("Background/Foreground/CardName").set_text(hand[i].name)
		card.get_node("Background/Foreground/RoomType").set_text("Combat")
		card.get_node("Background/Foreground/RoomLevel").set_text(str(hand[i].level))
		card.get_node("Click").gameHandler = gameHandler
		card.get_node("Click").script = hand[i]
		if(playingField != null):
			playingField.add_child(card)
	
func Process(delta):
	if(playingField != null):
		for i in range(playingField.get_child_count()):
			if(!playingField.get_child(i).is_queued_for_deletion()):
				var width = playingField.get_child(i).get_node("Background").get_margin(MARGIN_RIGHT)
				playingField.get_child(i).set_pos(Vector2(lerp(0, i * width + (width / 2) + (i * 3), lerpTick), lerp(800, 150, lerpTick)))
		
		lerpTick = min(1, lerpTick + (delta * 2))
	
func Exit():
	entity.RoomReturn()
	for i in range(playingField.get_child_count()):
		playingField.remove_child(playingField.get_child(0))
		#playingField.get_child(0).queue_free()