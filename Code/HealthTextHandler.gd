extends Label

var entity

func _ready():
	set_process(true)
	
func _process(delta):
	if(entity == null):
		return
	
	self.set_text(str(entity.HPRemaining) + "/" + str(entity.HP))