extends Node

var treasures = []

func _init():
	var attackHandler = load("res://Code/AttackHandler.gd").new()
	for i in attackHandler.rawJson.keys():
		if(attackHandler.rawJson[i].attack.treasure == "true"):
			treasures.push_back(attackHandler.attacks[i])
	
func GetTreasureOfLevel(level):
	var valids = []
	for i in treasures:
		if(i.level == level):
			valids.push_back(i)
			
	var result = randi() % valids.size()
	return valids[result]