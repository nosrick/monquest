extends Node

const XP_PER_LEVEL = 10

var HPRemaining
var HP

var HPPerLevel
var level
var XP

var roomDeck = []
var usedRooms = []

var attackDeck = []
var usedAttacks = []

var hand = []

var initiative = 0

var isPlayerControlled

var name

var soundPlayer

var isLeveling = false

func _init(HPPerLevel, level, rooms, attacks, player, name, soundPlayer):
	self.level = level
	self.HPPerLevel = HPPerLevel
	self.isPlayerControlled = player
	self.name = name
	self.soundPlayer = soundPlayer
	
	HP = level * HPPerLevel
	HPRemaining = HP
	
	XP = 0
	
	roomDeck = rooms
	attackDeck = attacks
	
func SetInitiative(value):
	initiative = value
	
func PushBack(value):
	initiative += value
	
func PullForward(value):
	initiative = max(0, initiative - value)
	
func DamageMe(value):
	HPRemaining = max(0, HPRemaining - value)
	
func HealMe(value):
	HPRemaining = min(HP, HPRemaining + value)
	
func AddXP(value):
	XP += value
	if(XP >= XP_PER_LEVEL * level):
		XP -= XP_PER_LEVEL * level
		LevelUp()

func LevelUp():
	level += 1
	HP += HPPerLevel
	HPRemaining += HPPerLevel
	isLeveling = true

func InitialAttackDraw():
	for i in range(4):
		AttackDraw()
	
func AttackDraw():
	if(attackDeck.size() > 0):
		hand.push_back(attackDeck[0])
		attackDeck.pop_front()
	
func AttackReturn():
	for i in hand:
		attackDeck.push_back(i)
		
	hand.clear()
		
	for i in usedAttacks:
		attackDeck.push_back(i)
	
	usedAttacks.clear()

func InitialRoomDraw():
	for i in range(3):
		RoomDraw()
		
func RoomDraw():
	if(roomDeck.size() > 0):
		hand.push_back(roomDeck[0])
		roomDeck.pop_front()

func RoomReturn():
	for i in hand:
		roomDeck.push_back(i)
		
	hand.clear()
		
	for i in usedRooms:
		roomDeck.push_back(i)
		
	usedRooms.clear()
	

func Shuffle(deck):
	var i = deck.size()
	while i > 1:
		i -= 1
		var j = randi() % i
		var temp = deck[i]
		deck[i] = deck[j]
		deck[j] = temp
	

func RollCheck():
	var result = range(1, HPRemaining + 1)
	if(result < HPRemaining - (HP / 10)):
		return true
	
	return false