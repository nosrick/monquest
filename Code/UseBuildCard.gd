extends Control

var gameHandler
var script

func _ready():
	set_process_input(true)
	connect("mouse_exit", self, "_on_mouse_exit")
	
func _input_event(event):
	get_parent().set_scale(Vector2(1.1, 1.1))
	
	if(event.is_action_released("left_click")):
		if(gameHandler != null && gameHandler.currentRoom == null):
			gameHandler.currentRoom = script
			#gameHandler.players[gameHandler.buildingTurn].usedRooms.push_back(script)
			gameHandler.ChangeState(gameHandler.placeRoomStateScript.new(script))
			

func _on_mouse_exit():
	get_parent().set_scale(Vector2(1, 1))