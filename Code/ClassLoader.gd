extends Object

var entityResource = preload("Entity.gd")

var warrior
var thief
var wizard
var cleric

var soundPlayer

func _init():
	pass
	
func LoadDefaults():
	#Do the warrior first
	warrior = LoadClass("Warrior", true)
	
	#Next comes the thief	
	thief = LoadClass("Thief", true)
	
	#Next is the wizard
	wizard = LoadClass("Wizard", true)
	
	#Lastly is the cleric
	cleric = LoadClass("Cleric", true)
	
func LoadClass(className, playerControlled = false):
	var file = File.new()
	file.open("res://Content/Data/Class Definitions/" + className+ ".json", File.READ)
	var jsonString = file.get_as_text()
	var jsonData = {}
	jsonData.parse_json(jsonString)
	
	var attackLoader = load("res://Code/AttackProvider.gd").new()
	var attacks = attackLoader.GetAttacks(1, jsonData)
	
	var roomLoader = load("res://Code/RoomProvider.gd").new()
	var rooms = roomLoader.GetRooms(1, jsonData)
	
	var classData = entityResource.new(int(jsonData.entity.HPPerLevel), 1, rooms, attacks, playerControlled, className, soundPlayer)
	
	file.close()
	
	return classData