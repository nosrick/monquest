extends Object

var attackHandler

func GetAttacks(entityLevel, jsonData):
	var entityData = jsonData
	attackHandler = load("res://Code/AttackHandler.gd").new()
	
	var attacks = []
	for i in entityData.entity.attacks:
		var internalName = i.internalName
		var level = int(i.level)
		var attack = attackHandler.GetAttack(internalName)
		if(level == 0):
			for j in range(5):
				attacks.push_back(attack)
				
		elif(level <= entityLevel):
			attacks.push_back(attack)
		
	
	return attacks
	
func GetLevelUpAttacks(entityLevel, entityClass):
	var file = File.new()
	file.open("res://Content/Data/Class Definitions/" + entityClass + ".json", File.READ)
	attackHandler = load("res://Code/AttackHandler.gd").new()
	var jsonString = file.get_as_text()
	var jsonData = {}
	jsonData.parse_json(jsonString)
	
	var attacks = {}
	for i in jsonData.entity.attacks:
		var internalName = i.internalName
		var level = int(i.level)
		
		if(level == 0):
			continue
			
		var attack = attackHandler.GetAttack(internalName)
		
		if(level <= entityLevel):
			attacks[level] = attack
			
	return attacks