extends "GameState.gd"

var currentEntity
var entityIndex
var points

var attackProvider = preload("AttackProvider.gd").new()
var classLoader = preload("ClassLoader.gd").new()
var cardResource = preload("../LevelUpCard.scn")

var lerpTick = 0
var label

func _init():
	self.name = "levelup"

func Enter(gameHandler, playingField):
	.Enter(gameHandler, playingField)
	entityIndex = 0
	currentEntity = gameHandler.players[entityIndex]
	points = currentEntity.level
	label = Label.new()
	playingField.add_child(label)
	label.set_pos(Vector2(200, 460))
	label.add_font_override("font", ResourceLoader.load("res://Content/Fonts/SmallFont.fnt"))
	
func Process(delta):
	if(playingField != null):
		for i in range(playingField.get_child_count()):
			if(playingField.get_child(i) == label):
				continue
			
			var width = playingField.get_child(i).get_node("CardBackground").get_margin(MARGIN_RIGHT)
			playingField.get_child(i).set_pos(Vector2(lerp(0, i * width + (width / 2) + (i * 3), lerpTick), lerp(800, 150, lerpTick)))
		
		lerpTick = min(1, lerpTick + (delta * 2))
	
func DisplayCards():
	for i in playingField.get_children():
		if(i != label):
			i.free()
		
	currentEntity = gameHandler.players[entityIndex]
	
	if(!currentEntity.isLeveling or points == 0):
		entityIndex += 1
		
		if(entityIndex == 4):
			gameHandler.ChangeState(gameHandler.buildingStateScript.new())
			return
		
		currentEntity = gameHandler.players[entityIndex]
		if(!currentEntity.isLeveling):
			DisplayCards()
			return
		
		points = currentEntity.level
	label.set_text(str(points) + " points remaining")
	
	var hand = attackProvider.GetLevelUpAttacks(currentEntity.level, currentEntity.name)
	
	for i in range(1, hand.size() + 1):
		if(points >= i):
			var card = cardResource.instance()
			var width = card.get_node("CardBackground").get_margin(MARGIN_RIGHT)
			card.get_node("CardBackground/CardForeground/CardName").set_text(hand[i].name)
			card.get_node("CardBackground/CardForeground/Level").set_text(str(hand[i].level))
			card.get_node("CardBackground/CardForeground/Power").set_text(str(hand[i].CalculatePower(currentEntity, null)))
			card.get_node("CardBackground/CardForeground/Initiative").set_text(str(hand[i].pushback))
			card.get_node("CardBackground/CardForeground/Description").set_text(hand[i].description)
			card.get_node("Click").gameHandler = gameHandler
			card.get_node("Click").script = hand[i]
			card.get_node("Click").user = currentEntity
			card.get_node("Click").gameState = self
			card.get_node("Click").level = i
			if(playingField != null):
				playingField.add_child(card)
				
	lerpTick = 0
	
func HandleInput(event):
	pass
	
func Exit():
	for i in playingField.get_children():
		i.queue_free()
		
	for i in gameHandler.players:
		i.isLeveling = false