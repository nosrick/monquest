extends Camera2D

var cameraSpeed = 10
var lastPosition = Vector2(0, 0)

func _ready():
	set_process(true)
	set_process_input(true)
	var board = get_tree().get_root().get_node("Game/Board")
	set_limit(MARGIN_TOP, -200)
	set_limit(MARGIN_BOTTOM, board.mapSize * board.size + 200)

func _input(event):
	if(event.type == InputEvent.MOUSE_BUTTON):
		var zoom = get_zoom()
		if(event.button_index == BUTTON_WHEEL_UP):
			set_zoom(Vector2(zoom.x - 0.05, zoom.y - 0.05))
			
		if(event.button_index == BUTTON_WHEEL_DOWN):
			set_zoom(Vector2(zoom.x + 0.05, zoom.y + 0.05))
			

func _process(delta):
	var position = get_pos()
	if(Input.is_mouse_button_pressed(BUTTON_RIGHT)):
		var mousePosition = get_viewport().get_mouse_pos()
		var different = Vector2((mousePosition.x - lastPosition.x) * -1, (mousePosition.y - lastPosition.y) * -1)
		set_pos(Vector2(position.x + different.x, position.y + different.y))
		
	lastPosition = get_viewport().get_mouse_pos()