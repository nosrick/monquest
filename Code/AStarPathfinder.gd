extends Object

var AStarNode = preload("AStarNode.gd")

func FindPath(begin, end, grid):
	var openList = []
	var closedList = []
	var solution = []
	var beginNode = AStarNode.new(null, begin, 0)
	var endNode = AStarNode.new(null, end, 0)
	var loopBreak = 0
	openList.push_back(beginNode)
	while(openList.size() > 0 and loopBreak < 1000):
		var currentNode = openList[0]
		openList.pop_front()
		
		if(currentNode.position == end):
			var index = 0
			while(currentNode != null):
				solution[index] = currentNode
				currentNode = currentNode.parent
				index += 1
			return solution
			
		currentNode.successors = currentNode.GetSuccessors()
		for node in currentNode.successors:
			var nodeOpen = null
			if(HasNode(openList, node)):
				nodeOpen = node
				if(node.Calculate(endNode) > nodeOpen.Calculate(endNode)):
					continue
				
			var nodeClosed = null
			if(HasNode(closedList, node)):
				nodeClosed = node
				if(node.Calculate(endNode) > nodeClosed.Calculate(endNode)):
					continue
			
			openList.erase(nodeOpen)
			closedList.erase(nodeClosed)
			openList.push_back(node)
			
			
		closedList.push_back(currentNode)
		loopBreak += 1
		
	
	
func HasNode(array, node):
	for searchNode in array:
		if(searchNode == node):
			return true
		
	return false