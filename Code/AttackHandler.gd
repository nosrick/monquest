extends Object

var attacks = {}
var rawJson = {}

func _init():
	var directory = Directory.new()
	var attackDirectory = directory.get_current_dir() + "Code/Attacks"
	var jsonDirectory = directory.get_current_dir() + "Content/Data/Attack Definitions"

	var jsonData = {}
	
	directory.change_dir(jsonDirectory)
	directory.list_dir_begin()
	var file = str("")
	file = directory.get_next()
	while(file != ""):
		if(file != "." and file != ".."):
			var name = file.substr(0, file.length() - 5)
			var fileData = File.new()
			fileData.open(jsonDirectory + "/" + file, File.READ)
			jsonData[name] = fileData.get_as_text()
		
		file = directory.get_next()
		
	
	
	directory.change_dir(attackDirectory)
	directory.list_dir_begin()
	file = str("")
	file = directory.get_next()
	while(file != ""):
		if(file != "." and file != ".." and !file.begins_with("BaseAttack")):
			var name = file.substr(0, file.length() - 3)
			var attack = load("res://Code/Attacks/" + file)
			var attackJson = {}
			attackJson.parse_json(jsonData[name])
			rawJson[name] = attackJson
			attacks[name] = attack.new(attackJson.attack.name, attackJson.attack.internalName, attackJson.attack.pushback, attackJson.attack.sounds, attackJson.attack.level, attackJson.attack.description)
			
		file = directory.get_next()
		

func GetAttack(name):
	if(attacks.has(str(name))):
		return attacks[name]