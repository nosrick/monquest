
extends Object

var textures = {}

func Load(path = "res://Content/Sprites/Uncut/"):
	var directory = Directory.new()
	var baseDirectory = path
	print("Got path ", baseDirectory)
	
	directory.change_dir(baseDirectory)
	directory.list_dir_begin()
	var temp = directory.get_next()
	while temp != "":
		if(temp == "." or temp == ".."):
			temp = directory.get_next()
			continue
		"""
		if(directory.current_is_dir()):
			print("Got directory, ", temp)
			Load(baseDirectory + "/" + temp)
		else:
			"""
		print("Got file, ", temp)
		var extension = temp.substr(temp.find_last("."), 4)
		#var fullPath = directory.get_current_dir() + "/" + temp
		#print("Full path: ", fullPath)
		if(extension == ".png"):
			var name = temp.substr(0, temp.length() - 4)
			textures[name] = ResourceLoader.load(baseDirectory + "/" + temp)
			print("Loaded ", name)
		
		temp = directory.get_next()
	
func Get(name):
	if(textures.has(name)):
		return textures[name]